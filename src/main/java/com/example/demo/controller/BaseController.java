package com.example.demo.controller;

import com.example.demo.entity.BaseEntity;
import com.example.demo.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

/**
 * @author jakub
 * 15.04.2023
 */
public abstract class BaseController<T extends BaseEntity<ID>, ID extends Serializable, DTO, S extends BaseService<T, ID, ? extends JpaRepository<T, ID>>> {

    @Autowired
    protected S service;
}
