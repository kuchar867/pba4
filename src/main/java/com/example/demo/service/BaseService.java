package com.example.demo.service;

import com.example.demo.entity.BaseEntity;
import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.repository.BaseRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * @author jakub
 * 14.01.2022
 */
@Transactional
public abstract class BaseService<T extends BaseEntity<ID>, ID extends Serializable, R extends BaseRepository<T, ID>> {

    protected static final Log logger = LogFactory.getLog(BaseService.class);

    @Autowired
    protected R repository;

    public List<T> findAll() {
        logger.info(String.format("Sending all entities from class %s",
                this.getClass().getSimpleName().substring(
                        0,
                        this.getClass().getSimpleName().indexOf("Service"))
        ));
        return repository.findAll();
    }

    public T findById(ID id) {
        Optional<T> entity = repository.findById(id);
        if (entity.isEmpty()) {
            throw new EntityNotFoundException();
        }
        logger.info(String.format("Sending entity with id %s", entity.get().getId()));
        return entity.get();
    }

    public T delete(ID id) {
        Optional<T> entity = repository.findById(id);
        if (entity.isEmpty()) {
            throw new EntityNotFoundException();
        }
        beforeDelete(entity.get());
        repository.delete(entity.get());
        logger.info(String.format("Deleting entity with id %s", entity.get().getId()));
        afterDelete(id);
        return entity.get();
    }

    public T save(T entity) {
        beforeSave(entity);
        logger.info("Saving new entity");
        return repository.save(entity);
    }

    public T update(ID id, T updatedEntity) {
        Optional<T> entity = repository.findById(id);
        if (entity.isEmpty()) {
            throw new EntityNotFoundException();
        }
        updatedEntity.setId(entity.get().getId());

        logger.info(String.format("Updating entity with id %s", updatedEntity.getId()));
        return repository.save(updatedEntity);
    }

    protected void beforeSave(T entity) {

    }

    protected void beforeDelete(T entity) {

    }

    protected void afterDelete(ID id) {

    }
}
